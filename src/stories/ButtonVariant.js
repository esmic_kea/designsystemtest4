import React from 'react';
import PropTypes from 'prop-types';
import './button.css';

/**
 * Primary UI component for user interaction
 */
export const ButtonVariant = ({ primary, backgroundColor, size, label, ...props }) => {
  const mode = primary ? 'storybook-buttonvariant--primary' : 'storybook-buttonvariant--secondary';
  return (
    <button
      type="button"
      className={['storybook-button', `storybook-button--${size}`, mode].join(' ')}
      style={backgroundColor && { backgroundColor }}
      {...props}
    >
      {label}
    </button>
  );
};

ButtonVariant.propTypes = {
  /**
   */
  primary: PropTypes.bool,
  /**
   * What background color to use
   */
  backgroundColor: PropTypes.string,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**
   * Button contentsx
   */
  label: PropTypes.string.isRequired,
  /**
   * Optional click handlerxxxsdf
   */
  onClick: PropTypes.func,
};

ButtonVariant.defaultProps = {
  backgroundColor: 'black',
  primary: false,
  size: 'large',
  onClick: undefined,
};
